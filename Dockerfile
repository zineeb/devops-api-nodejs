FROM node:21-alpine

COPY package*.json ./

WORKDIR usr/src

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "node", "src/app.js" ]
